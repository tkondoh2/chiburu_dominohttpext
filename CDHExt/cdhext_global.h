#ifndef CDHEXT_GLOBAL_H
#define CDHEXT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CDHEXT_LIBRARY)
#  define CDHEXTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CDHEXTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CDHEXT_GLOBAL_H
